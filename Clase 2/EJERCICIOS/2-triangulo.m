%Ingresar 3 datos y decir si es un tri�ngulo Is�celes, Escaleno o
%Equil�tero. Calcular el �rea y per�metro teniendo en cuenta que el �rea es
%b*h/2, donde la base es el tercer lado y el per�metro de un tri�ngulo 
%equilatero es 3*a, isoceles es 2*a+b y escaleno es a+b+c.

%Tener en cuenta que el area de un triangulo escaleno es la raiz de
%s(s-a)(s-b)(s-c) donde s = a+b+c/2
clc, clear
x = input('Ingrese el primer lado: ');
y = input('Ingrese el segundo lado: ');
z = input('Ingrese el tercer lado (base): ');


if(x == y && y == z)
    altura = sqrt(3)*x/2;
    area = z*altura/2;
    perim = 3*x;
    fprintf("Es un tri�ngulo equil�tero. \n");
    fprintf("El �rea es: %10.4f \n", area);
    fprintf("El per�metro es es: %10.4f \n", perim);
elseif(x == y || x == z || z == y)
    
    altura = sqrt(x^2 - z^2/4);
    area = z*altura/2;
    perim = 2*x+y;
    fprintf("Es un tri�ngulo is�seles. \n");
    fprintf("El �rea es: %10.4f \n", area);
    fprintf("El per�metro es es: %10.4f \n", perim);

else
    s = (x+y+z)/2;
    area = sqrt(s*(s-x)*(s-y)*(s-z));
    perim = x+y+z;
    fprintf("Es un tri�ngulo escaleno. \n");
    fprintf("El �rea es: %10.4f \n", area);
    fprintf("El per�metro es es: %10.4f \n", perim);
end  