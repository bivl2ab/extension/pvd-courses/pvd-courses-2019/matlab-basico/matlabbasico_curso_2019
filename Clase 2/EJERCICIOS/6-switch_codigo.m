clc, clear

flag = 0;
while flag == 0
    fprintf("\n");
    fprintf("-------------------------------\n");
    fprintf("----Bienvenido al programa ----\n");
    fprintf("----1. Calcular factorial------\n");
    fprintf("----2. Calcular n�mero primo---\n");
    fprintf("----3. Calcular n�mero par-----\n");
    fprintf("----4. Salir-------------------\n");
    fprintf("-------------------------------\n");
    num = input('Ingrese el n�mero que desee: ');
    switch num
        case 1
            f = input('Ingrese el numero del que desea calcular el factorial: ');
            facto = 1;
            for i=1:f
                facto = facto*i;
            end
            fprintf('El factorial del %d es: %d \n', f, facto)
        case 2
            n = input('Ingrese el numero que quiere saber si es primo o no: ');
            cantidad = 0;
            if(n ~= 1)
                for i=1:n
                    if(mod(n,i) == 0)
                        cantidad = cantidad + 1;
                    end
                end

                if(cantidad == 2)
                    fprintf("El numero %d es un numero primo. \n", n);
                else
                    fprintf("El numero %d no es un numero primo. \n", n);
                end
            else
                fprintf("El numero %d no es un numero primo. \n", n);    
            end
            
        case 3
            par = input('Ingrese el numero para saber si es par: ');
            if (mod(par,2) == 0)
                fprintf("El numero %d es par \n", par);
            else
                fprintf("El numero %d no es par \n", par);
            end
        case 4
            fprintf("Muchas gracias por ingresar al programa, Adi�s.. \n");
            flag = 1;
        otherwise
            fprintf("Ingrese un n�mero valido");
            
    end
end