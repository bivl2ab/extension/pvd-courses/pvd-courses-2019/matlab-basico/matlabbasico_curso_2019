%Crear un vector random flotante de longitud n. Mu�vase elemento a elemento
%del vector y si el valor randomico es menor a 0.3 convi�rtalo en 0, si es
%mayor que 0.4 convi�rtalo en 1 y si est� entre 0.3 y 0.4 convi�rtalo en 0.5

n = input('Ingrese la longitud del vector que desea tener: ');
random = rand([1,n])
for i=1:length(random)
    if(random(i) < 0.3)
        random(i) = 0;
    elseif (random(i) > 0.4)
        random(i) = 1;
    elseif (random(i) >= 0.3) && (random(i)<=0.4)
        random(i) = 0.5;
    end
end
random