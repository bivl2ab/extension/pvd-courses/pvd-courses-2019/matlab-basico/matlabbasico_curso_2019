%Escriba un programa que:
%1. Un vector que imprima los n�meros hasta que la suma de todos estos sea
%mayor que 20.
%2. Un vector que imprima los n�meros hasta encontrar un n�mero random 30
%3. Un vector que imprima los n�meros hasta encontrar un n�mero random
%entre 0.8 y 0.85
%4. Un vector que imprima los n�meros hasta encontrar una media que al
%restarla con 0.5 sea menor a 0.001 (aproximaci�n a una media igual a 0.5).
%Hacerlo de la misma manera con desviaci�n estandar pero debe ser menor a
%0.01

%%
cont = 1; suma = 0;
vector = [];
while(suma <= 20)
    x = randi(10);
    suma = suma + x;
    vector(cont) = x;
    cont = cont+1;
end
fprintf("Tomo %d numeros para que la suma sea %10.6f. El vector es: \n", cont-1, suma);
disp(vector)
%%
cont = 1; r = 0;
random = [1 1];
while(r ~= 30)
    r = randi(100);
    random(cont) = r;
    cont = cont+1;
end
fprintf("Tomo %d numeros para que apareciera el n�mero 30. El vector es: \n", cont-1);
disp(random)
%%
cont = 1; random = [1 1];
while(1)
    r = rand();
    random(cont) = r;
    cont = cont+1;
    if(r <0.85 && r > 0.8)
        break
    end
end
fprintf("Tomo %d numeros para que apareciera un n�mero entre [0.8,0.85]. El vector es: \n", cont-1);
disp(random)
%%
cont = 1; avg = 0;
random = 0;
while(abs(avg - 0.5) > 0.001)
    r = rand();
    random(cont) = r;
    avg = sum(random)/cont;
    cont = cont+1;
end
fprintf("Tomo %d numeros para que la media fuese 0.5. Con la media %10.6f, el vector es: \n", cont-1, avg);
disp(random)
%%
cont = 1; desv = 0;
random = [0 0];
while(abs(desv - 0.2) > 0.01)
    r = rand();
    random(cont) = r;
    desv = std(random);
    cont = cont+1;
end
fprintf("Tomo %d numeros para que la desviaci�n estandar fuese aprox 0.3. Con resultado %10.6f, el vector es: \n", cont-1, desv);
disp(random)