vector = [20,16,30,100,55];
fprintf('El vector desordenado es: ');
disp(vector)
aux = 0;
for j=1:length(vector)-1
    for i=1:length(vector)-1
        if(vector(i)>vector(i+1))
            aux = vector(i);
            vector(i) = vector(i+1);
            vector(i+1) = aux;
        end
    end
end
fprintf('El vector ordenado es: ');
disp(vector)