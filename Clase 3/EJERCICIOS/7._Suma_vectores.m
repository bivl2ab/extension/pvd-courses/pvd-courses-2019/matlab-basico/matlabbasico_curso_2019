%Dados dos vectores, sumarlos entre componentes, calcular el promedio y la
%desviación estandar (para la ultima use std()). Crear un vector para cada
%uno.
u = [pi, cos(90), sin(30), log(2), exp(2)];
v = [cos(30), sin(45), log(5), 4^2, 24];
suma = zeros([1,length(u)]);
prom = zeros([1,length(u)]);
desv = zeros([1,length(u)]);
for i=1:length(u)
    suma(i) = u(i) + v(i);
    prom(i) = (u(i) + v(i))/2;
    desv(i) = std([u(i), v(i)]);
end
suma
prom
desv