%Se pueden separar por comas o por espacio

%Matrices
a = [1 2 3; 4 5 6; 7 8 9; 10 11 12];

%Acceder al elemento situado en la fila 3 columna 3
a(3,3);

%Acceder a algunos elementos de la matriz
%Posici�n 1,2 en filas, y 3 en columnas
a([1,2],3)

%Ejercicio, posici�n 2 en filas, 1,2,3 en columnas
%

%a(2,[2,3,4])
%a([2,3],2:4)
%Eliminar elementos de la matriz 
a(4,:) = [];

%Dimensiones de la matriz a (4 filas, 3 columnas)
size(a);

%Sacarle la longitud de la matriz
length(a) 

%Crear vectores fila
b_1=[1,2,3]; 
b_2=[4,5,6]; 
b=[b_1;b_2];


%Crear vectores columna 
c_1=[1;2;3]; 
c_2=[4;5;6];
c=[c_1,c_2];

%Ejercicio, crear dos matrices, uno de 1 a 6 (2 filas, 3 columnas)
%y otro de 7 a 15 (3 filas, 3 columas) y concatenarlos.
X=[1,2,3;4,5,6];
Y=[7,8,9;10,11,12;13,14,15];
Z=[X;Y];

%Crear matrices de ceros
d = zeros(3);
%Crear matrices identidad
e = eye(3);
%Crear matrices de unos
f = ones(3);

%Suma de las columnas de Z
sum(Z);

%Suma de las componentes de z
sum(sum(Z));

%Suma de una matriz por un escalar
suma = 4+Z;

%Producto de una matriz por un vector
producto = 2*Z;

%Calcula el producto de las columnas de Z
Z;
prod(Z);

%Calcula el producto de las componentes de Z
prod(prod(Z));

%Crear una matriz de n�meros aleatoreos flotantes
e = rand(5);

%Crear una matriz de n�meros aleatoreos enteros
r = randi(26,5);