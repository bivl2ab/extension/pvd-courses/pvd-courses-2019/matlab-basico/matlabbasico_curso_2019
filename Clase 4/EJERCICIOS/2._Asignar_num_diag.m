%Realizar un programa que en la diagonal me agregue dos, debajo y arriba de
%la diagonal me agregue un -1 y el resto que sean ceros


n_fil = input('Ingrese la cantidad de filas que desea: ');
n_col = input('Ingrese la cantidad de columnas que desea: ');
A = ones(n_fil,n_col);

for f = 1:n_fil
    for c = 1:n_col
        if f == c
            A(f,c) = 2;
        elseif abs(f-c) == 1
            A(f,c) = -1;
        else
            A(f,c) = 0;
        end
    end
end
A