%Dadas dos matrices n,m siendo el primer valor de cada una de ellas es la 
%clave, basarse de la matriz m

datos_a = [1, 120; 1, 130; 2, 140; 3, 180; 3, 160];
datos_b = [1, 91; 2, 92; 3, 93];
[fila, columna] = size(datos_a);
[h, w] = size(datos_b);
for i=1:fila
    for k=1:h
        if(datos_a(i,1) == datos_b(k,1))
            datos_a(i,3) = datos_b(k,2);
        end
    end
end
datos_a