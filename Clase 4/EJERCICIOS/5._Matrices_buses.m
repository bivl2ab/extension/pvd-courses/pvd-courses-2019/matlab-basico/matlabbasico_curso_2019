%Alicante es una empresa de transporte tiene que 30 buses, los cuales 
%presentan ingresos hasta de $4'000.000 por mes. El due�o de este, quiere
%saber cu�ntos ingresos obtuvo cada uno de ellos en el a�o. Luego de esto, 
%se da cuenta que quiere premiar a dos buses. El primero, es en el que en 
%promedio tiene el ingreso m�s alto por mes y el segundo, aquel que en todo
%el a�o fue el que m�s produjo. De la misma manera, quiere saber cu�l fue 
%el que tuvo mayor desviaci�n est�ndar en el a�o y los ingresos totales por mes.

ingresos = randi(4000000, 30, 12);
[filas, columnas] = size(ingresos);
promedio_ingresos = zeros(1,30);
suma_ingresos = zeros(1,30);
desviacion_ingresos = zeros(1,30);
ingresos_mes = zeros(1,12);
meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
for i=1:filas
    promedio_ingresos(i) = mean(ingresos(i,:));
    suma_ingresos(i) = sum(ingresos(i,:));
    desviacion_ingresos(i) = std(ingresos(i,:));
    for j=1:columnas
        ingresos_mes(j) = sum(ingresos(:,j));
    end
end
promedio_mayor = 0; suma_mayor = 0; desv_mayor=0;
bus_prom = 0; bus_sum = 0; bus_desv = 0;
for i=1:filas
    if(promedio_ingresos(i) > promedio_mayor)
        promedio_mayor = promedio_ingresos(i);
        bus_prom = i;
    end
    if(suma_ingresos(i) > suma_mayor)
        suma_mayor = suma_ingresos(i);
        bus_sum = i;
    end
    if(desviacion_ingresos(i) > desv_mayor)
        desv_mayor = desviacion_ingresos(i);
        bus_desv = i;
    end
end

fprintf("El bus %d es quien en promedio gana m�s por mes, siendo el ingreso de %10.1f\n", bus_prom, promedio_mayor);
fprintf("El bus %d es quien en gan� m�s en el a�o, siendo el ingreso de %10.1f\n", bus_sum, suma_mayor);
fprintf("El bus %d es quien obtuvo un peor desempe�o en el a�o, siendo la desviaci�n estandar de %10.1f\n", bus_desv, desv_mayor);
fprintf("------------------------------------------------------------------------------------------------\n")
for i=1:columnas
    fprintf("En el mes %s se obtuvo en total un ingreso de %d \n", meses(i), ingresos_mes(i));
end