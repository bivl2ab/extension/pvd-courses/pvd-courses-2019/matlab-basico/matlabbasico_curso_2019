%Z es una matriz de nxm la cual tiene enteros de 1 a 255 hay solo K enteros 
%en la matriz, donde K < 255. Realice un programa que mapee los enteros de 
%la matriz del rango(1, 255) a (1,K).

z = [1, 100, 75; 150, 233, 255; 172, 201, 54];
[filas, columnas] = size(z);
valor = 1;
for i=1:filas
    for j=1:columnas
        if(z(i,j) == 1)
            z(i,j) = 1;
        else
            z(i,j) = round(z(i,j)*(filas*columnas)/255);
        end
    end
end

z