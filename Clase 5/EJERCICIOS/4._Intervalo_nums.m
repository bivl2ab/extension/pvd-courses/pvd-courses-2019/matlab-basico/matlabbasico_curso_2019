%Dados dos n�meros, mostrar el intervalo de n�mero enteros que van desde el
%primer n�mero al segundo. Controlando que los valores sean los correctos.
% Lo que debe imprimir ser�n, los n�meros pares elevados al cuadrado y su
% respectivo factorial
clc, clear
x = input('Ingrese el primer numero ');
y = input('Ingrese el segundo numero ');

if(x > y)
    aux = x;
    x = y;
    y = aux;
end
cont = 1;
facto = [];
cuadr = [];
valor = []
for i=x:y
    if(mod(i,2) == 0)
        valor(cont) = i
        facto(cont) = factorial(i);
        cuadr(cont) = i^2
        %fprintf("El numero %d es par, elevado al cuadrado da %d y su respectivo factorial es: %d \n", i, i^2,facto);            
        cont = cont + 1;
    end
end