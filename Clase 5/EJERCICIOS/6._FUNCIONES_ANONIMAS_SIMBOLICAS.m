%Funciones simb�licas. 
%(simplificaci�n algebraica, soluci�n de ecuaciones diferenciales de forma 
%simb�lica, soluci�n de ecuaciones e inecuaciones algebraicas, etc...)
a = sym(pi); %Almacena la constante simb�lica pi
b = double(a); %Obtiene el valor num�rico de la variable a

%Convierte a variables simb�licas, o sea, un par�metro en una funci�n. No
%tienen valores en concreto.
syms x y 
func = 2*x^3 + y*3/2 + 3; %Realizando esto, se crea una variable simb�lica.

%Si en una expresi�n simb�lica queremos sustituir una variable por otra 
%o por una constante para calcular su valor en un punto utilizamos:
s = subs(func,x,20);

%Para sustituir dos variables..
s_v = subs(func,{x, y}, {20, 5});

%-------------------------------------------------------------------------
%EJERCICIO: Cree la funci�n simb�lica f = ax2+bx+c y cambie la variable
%simb�lica x por t. Luego de ello iguale a=3, b=34, c = 10 y obtenga el
%valor de la funci�n f cuando t = 4 y cuando t pertenece al intervalo [1,10]

syms x t a b c
f = a*x^2+b*x+c;
g = subs(f, x, t);
h = subs(g, {a,b,c}, {3,34,10});
ej_1val = subs(h, t, 4);
ej_1vec = subs(h, t, [1:10]);

%%
%Funciones an�nimas
%Es util cuando se tienen muchas variables y datos y se utilizar� una
%operaci�n para todas
parabola = @(x) x^2+2;

%Para definir dos variables
trig = @(x,y) sin(x)*cos(y) + x*y;

%Se pueden crear variables en la funci�n
a = 1.3; b = 3.2; c = 30; p = @(x) a*x.^2 + b*x + c;
%Dado que a, b y c tienen un valor asignado antes de calcular la funci�n p,
%no importa si se eliminan las variables, estas quedan guardadas dentro de
%la funci�n an�nima
clear a b c x = 3; y = p(x);

%Sin embargo si se quieren actualizar las variables, se deben definir de
%nuevo.


%M�ltiples funciones an�nimas
h = @(w) (integral(@(x) (w*x.^2 + w*x + w),0,1));

%Crear matrices de funciones an�nimas. Se debe tener cuidado con los
%espacios.
f = {@(x)x.^2;      @(y)y+10;      @(x,y)x.^2+y+10};

%Para evaluarlas, se hace dela siguiente manera
x = 50; y = 20; 
f{1}(x), f{2}(y), f{3}(x,y)
