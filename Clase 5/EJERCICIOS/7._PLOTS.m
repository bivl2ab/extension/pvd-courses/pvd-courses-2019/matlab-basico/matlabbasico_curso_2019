% Dibuja los pares de puntos (i,xi) donde xi son las componentes del vector
% e i son las posiciones
x = [-4, 5, 6, 7, 10];
plot(x)

%Para dibujar dos vectores (x,y):
y = [5, 2, -7, 4 32];
plot(x,y)

%Para dibujar una matriz (Dibuja por columnas
 A = [1 2 0.5; 2 4 -0.5; 3 9 0.5; 4 16 -0.5; 5 25 0.5];
 plot(A);
 
%Para dibujar una funci�n
z = 0:1:2;
M = [sin(pi*z); 0.5+0.5*z];
plot(M,z);

%Sin embargo, plot tiene muchos m�s par�metros
%plot(x,y,'o--g','LineWidth', 2, 'MarkerSize', 8,'MarkerEdgeColor', 'b', 'markerfacecolor', 'y')


%De la misma manera, se puede graficar m�s de una funci�n en el plot...
x = linspace(0,3,50); 
fun1 = exp(-x.^2); 
fun2 = (x.^2).*exp(-x.^2);
fun3 = x.*exp(-x.^2);
fun4 = exp(-x);
plot(x, fun1, '+-g', x, fun2, '*:k', x, fun3, 'o-.y', x, fun4, 'x'); 
%Se pueden colocar nombre y t�tulos
title('Funciones exponenciales'), xlabel('x'), ylabel('y')
legend('e^x^2', 'x^2*e^x^2', 'x*e^x^2','e^x')

%Se usa subplot para subdividir la ventana de gr�fica en una reticula de m
%filas y n columnas ----- subplot(m,n,p) o subplot(2,2,1)

x=0:0.0001:3;
subplot(2,2,1);
plot (x,sin(5*x)), title('Seno');
subplot(2,2,2);
plot (x,cos(30*x)), title('Coseno');
subplot(2,2,3);
plot(x,x.*exp(-x*3)), title('Exponencial');
subplot(2,2,4);
plot(x,log(-x*1/2+3)), title('Logaritmo');

%Para limitar el eje x
t=0:0.2:2*pi+0.2; x=sin(t); y=cos(t);
subplot(3,1,1); plot(x,y,'+-g'); 
subplot(3,1,2); plot(x,y,'o-r'); axis square;
subplot(3,1,3); plot(x,y,'*-c'); axis normal; grid;


%Se puede hacer de la misma forma con funciones simb�licas
syms x ; f=x^2;
vec = linspace(-1,1,10);
f_x = subs(f,x,vec);
plot(vec,f_x,'c--')
hold on
g=x ;g_x= subs(g,x,vec);
plot(vec,g_x,'b d')
xlabel('Eje x')
ylabel('Eje y')
title('Graficas de f y g')

%% EJERCICIO
x = [-5:0.1:5];
f_1 = x.^2;
subplot(2,3,1); plot(x, f_1, '-b'); axis normal; grid; title("Parabola")
f_2 = sin(x);
subplot(2,3,2); plot(x, f_2, '-c'); axis normal; grid; title("Seno")
f_3 = -(x+2).^2+2;
subplot(2,3,3); plot(x, f_3, '-g'); axis normal; grid; title("-(x+2)^2+2")
f_4 = sin(x.^2);
subplot(2,3,4); plot(x, f_4, '-y'); axis normal; grid; title("Seno^2")
f_5 = 1./x;
subplot(2,3,5); plot(x, f_5, '-r'); axis normal; grid; title("1/x")
f_6 =((x+1).*sin(x))./x.^2;
subplot(2,3,6); plot(x, f_6, '-m'); axis normal; grid; title("((x+1)*sin(x))/x^2")