%Leer una imagen
imagen = imread('gato.jpg');
imagen = rgb2gray(imagen);
%hacer un histograma
histog = histogram(imagen)
%Hacerle treshold
BW = im2bw(imagen,0.1);
%Mostar una imagen
imshow(BW)
%figure
%Otro metodo de threshold
level = graythresh(imagen);
BW_ = imbinarize(imagen,level);
imshow(BW_)
