function i = integrar(f, a, b, N)
if(a>b)
    aux = a;
    a = b;
    b = aux;
end
h = (b+a)/N;
x_i = a + h*(1:N-1);
i = h* ((f(a)+f(b))/2 + sum(f(x_i)));
end