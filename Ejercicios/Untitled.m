%Se pueden separar por comas o por espacio

%Vectores fila
x = [1 -2 3 -4 5];
y = [-6,7,-8,9,10];
z = [2 4 6 -8,3];

%Acceder a una posici�n
y(1); y(2); y(end);

%Sacarle la longitud del vector
length(z);

%Asignar un valor en una posici�n que est� fuera del rango del vector
x(6) = 1/3;
x(15)= pi;

%Vector columna
w = [1;3;5;7;9];

%Concatenar los vectores 
x_y = [x y];

%Eliminar una posici�n del vector
x_y(7) = [];

%Eliminar varias posiciones del vector
x_y(7:13) = [];

%
m = [1:100];
%�Qu� hace esta instrucci�n?
m(50:2:70);

%
h = [0:2:20];
%Devuelve las posici�nes exactas que le indico
%b([4,20,1,5])
%�Por qu� da incorrecto?
length(h);
h([4,11,1,5]);

%Tama�o de los vectores
size(h);

%Suma de los componentes de b
sum(h);

%Calcula el producto de los componentes de b
h;
prod(h);
%�por qu� da 0? 
%Intentar hacerlo de la manera correcta
prod(h(2:end));


%Crear un vector de ceros
%Donde 1 son filas y 5 son columnas
c = zeros(1,5);

%Crear un vector de unos
d = ones(1,4);

%Crear un vector u 
v = [4 10 -3 7 -1 0 8 13 -7 0];
u = v(1:3:end);

%Ejercicio: crear un vector u con las posiciones final, 5, 8, 1, 3
ej_u = v([end,5,8,1,3]);

%Crear un vector de n�meros aleatoreos flotantes
e = rand([1,5]);

%Crear un vector de n�meros aleatoreos enteros
%N�meros del 1-26, de 1 fila, con 8 columnas
r = randi(26,1,8);

%Ejercicio: crear un vector de tama�o 20, agregarle en la columna 30 un 9, 
%y de la columna 21 a la 29 agregarle un vector de 9 elementos

ej_a = randi(100,1,20);
ej_a(30) = 9;
ej_b = randi(100,1,9);
ej_a(21:29) = ej_b;

%Ejercicio, sacarle el seno, coseno, tangentey el log a el vector creado del
%ejercicio anterior
ej_sin = sin(ej_a);
ej_cos = cos(ej_a);
ej_tan = tan(ej_a);
ej_log = log(ej_a);

%Sean a=(1,2,3) y b=(-2,3,5)
%1. Sume 3 a cada elemento de a y divida cada elemento de b entre 2.
%2. Realiza las operaciones que se indican: a+b, a-b, a?b 
%3. Divide los elementos de a entre los elementos de b en general y punto a
%punto
%4. Un vector columna que contenga los n�meros impares entre 1 y 1000.
%5. Un vector fila que contenga los n�meros pares entre 2 y 1000.
%6. Si x=0:2:20, escribe el comando de MATLAB que eleva al cuadrado cada
%componente de x.
%7. Verifique si x=(1,3,2) e y=(-2 2 -2) son ortogonales.
a= [1 2 3]; b=[-2 3 5];
%
ej_1 = 3+a;
%
ej_2a = a+b;
ej_2b = a-b;
ej_2c = a.*b;
%
ej_3a = a/b;
ej_3b = a./b;
%
ej_4 = [1:2:1000];
%
ej_5 = [0:2:1000];
%
ej_6x = [0:2:20];
ej_6c = ej_6x.^2;
%
f = [1 3 2]; g = [-2 2 -2];
ej_7 = sum(f.*g);
